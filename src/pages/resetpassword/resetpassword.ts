import { Component,ViewChild } from '@angular/core';
import { AlertController,IonicPage, NavController, NavParams } from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-resetpassword',
  templateUrl: 'resetpassword.html',
})
export class ResetpasswordPage {

  @ViewChild('username') uname;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private fire: AngularFireAuth
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetpasswordPage');
  }

  alert(title: string,message: string){
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  sendResetPasswordLink(){
    this.fire.auth.sendPasswordResetEmail(this.uname.value)
    .then(data=>{
      console.log('data : ',data);
      this.alert('Info!','Password Reset Link is sent to your email address.');
      this.navCtrl.push( LoginPage );
    })
    .catch(error=>{
      console.log('error : ',error);
      this.alert("Error",error.message);
    });
  }

}
