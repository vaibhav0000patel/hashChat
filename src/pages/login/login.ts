import { Component,ViewChild } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, LoadingController  } from 'ionic-angular';

import { RegisterPage } from '../register/register';
import { LoggedinPage } from '../loggedin/loggedin';
import { ResetpasswordPage } from '../resetpassword/resetpassword';

import { AngularFireAuth } from 'angularfire2/auth';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  @ViewChild('username') uname;
  @ViewChild('password') password;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private fire: AngularFireAuth,
    public loadingCtrl: LoadingController,

  ) {
  }

  

  alert(title: string,message: string){
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  signInUser(){

    if(!this.validateEmail(this.uname.value)){
      this.alert("Error","Invalid Email Address!");
    }else{
      this.fire.auth.signInWithEmailAndPassword(this.uname.value,this.password.value)
      .then(data=>{
        console.log('data : ',data);
        this.alert('Info!','Success! you\'re logged in.');
        this.navCtrl.setRoot( LoggedinPage );
      })
      .catch(error=>{
        console.log('error : ',error);
        this.alert("Error",error.message);
      });
    }
  }
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
  }
  signUp(){

    this.navCtrl.push(RegisterPage);
  }

  resetPassword(){
    this.navCtrl.push(ResetpasswordPage);
  }

}
