import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { RegisterPage } from '../register/register';
import { LoggedinPage } from '../loggedin/loggedin';

import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  	constructor(
      public navCtrl: NavController,
      private fire: AngularFireAuth
    ){

  	}
    ionViewDidLoad() {
        this.fire.auth.onAuthStateChanged( user => {
          if (user) {
            this.navCtrl.setRoot(LoggedinPage);
          }
        });

    }
    signIn(){
      this.navCtrl.push(LoginPage);
    }
    signUp(){
      this.navCtrl.push(RegisterPage);
    }

}
