import { Component,ViewChild } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  @ViewChild('username') uname;
  @ViewChild('password') password;
  @ViewChild('cpassword') cpassword;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private fire: AngularFireAuth
  ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  alert(title: string,message: string){
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  signUpUser(){

    if(!this.validateEmail(this.uname.value)){
      this.alert("Error","Invalid Email Address!");
    }
    else if(this.password.value!=this.cpassword.value){
      this.alert("Error","Password doesn't match with Confirm Password.");
    }else{
      this.fire.auth.createUserWithEmailAndPassword(this.uname.value,this.password.value)
      .then(data => {
          this.alert("Info!","Registed");
          console.log('Registed',data);
      })
      .catch(error =>{
          this.alert("Error",error.message);
          console.log('Error', error);
      })
    }
  }

  signIn(){
    this.navCtrl.push(LoginPage);
  }


}
