import { Component,ViewChild,ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

@IonicPage()
@Component({
  selector: 'page-loggedin',
  templateUrl: 'loggedin.html',
})
export class LoggedinPage {

  username: string='';
  message: string='';
  _chatSubscription;
  messages: object[] = [];

  constructor(
    private fire: AngularFireAuth,
    public navCtrl: NavController,
    public navParams: NavParams,
    public db: AngularFireDatabase,
    @ViewChild('scrollMe') private myScrollContainer: ElementRef
  ){
      this.username = fire.auth.currentUser.email;

      this.db.list('/chat').valueChanges().subscribe( data => {
        console.log(data);
        this.messages = data;
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoggedinPage');
  }
    ngOnInit() {
       this.scrollToBottom();
    }
    ngAfterViewChecked() {
       this.scrollToBottom();
    }
    scrollToBottom(): void {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch(err) { }
    }



  sendMessage(){
    this.db.list('/chat').push({
      username: this.username,
      message: this.message
    })
    this.message = '';
  }
  signOut(){
    this.fire.auth.signOut().then(data => {
      this.navCtrl.setRoot(HomePage);
    });
  }


}
